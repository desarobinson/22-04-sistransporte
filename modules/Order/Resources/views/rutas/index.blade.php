@extends('tenant.layouts.app')

@section('content')

    <tenant-rutas-index :type-user="{{json_encode(Auth::user()->type)}}"></tenant-rutas-index>

@endsection
