@extends('tenant.layouts.app')

@section('content')

    <tenant-carretatractos-index :type-user="{{json_encode(Auth::user()->type)}}"></tenant-carretatractos-index>

@endsection
