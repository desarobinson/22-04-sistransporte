<?php

namespace Modules\Order\Models;

//use App\Models\Tenant\Catalogs\IdentityDocumentType;
use App\Models\Tenant\ModelTenant;

class Vehiculo extends ModelTenant
{

  //  protected $with = ['identity_document_type'];
 
    protected $fillable = [
        'placa',
        'marca',
        'modelo',
        'cargautil',
        'carreta',
        'color',
        'motor',
        'serie',
        'anofabri',
        
    ];


   // public function identity_document_type()
   // {
       // return $this->belongsTo(IdentityDocumentType::class, 'identity_document_type_id');
   // }

}
