<?php

namespace Modules\Order\Models;

//use App\Models\Tenant\Catalogs\IdentityDocumentType;
use App\Models\Tenant\ModelTenant;
use App\Models\Tenant\Establishment;
class Tracto extends ModelTenant
{

  //  protected $with = ['identity_document_type'];
 
    protected $fillable = [
        'conductor',
        'placa',
        'fecha',
        'responsable',
        'articulos',
        'tarjetap',
        'tarjetac',
        'soat',
        'revision',
        'bonificacion',
        'formatos',
        'imagen1',
        'imagen2',
        'imagen3',
        'alarma',
        'botiquin',
        'elementos',
        'gata',
        'llaves',
        'autoradio',
        'nagua',
        'naceite',
        'luces',
        'frenos',
        'embrague',
        'aguaaceite',
        'tacografo',
        'neblineros',
        'vigia',
        'aire',
        'llanta1',
        'llanta2',
        'llanta3',
        'llanta4',
        'llanta5',
        'llanta6',
        'llanta7',
        'llanta8',
        'llanta9',
        'llanta10',
        'desc01',
        'desc02',
        'desc03',
        'combustibleactual',
        'combustiblentrega',
        'observaciones',
        'km',
        'establishment_id',

        
    ];
    protected $casts = [
      
    
  ];
    public function establishment()
    {
        return $this->belongsTo(Establishment::class);
    }

   // public function identity_document_type()
   // {
       // return $this->belongsTo(IdentityDocumentType::class, 'identity_document_type_id');
   // }

}
