<?php
namespace Modules\Order\Http\Controllers;

use Modules\Order\Http\Requests\CarretatractoRequest;
use Modules\Order\Http\Resources\CarretatractoCollection;
use Modules\Order\Http\Resources\CarretatractoResource;
use App\Models\Tenant\Catalogs\IdentityDocumentType;
use App\Http\Controllers\Controller;
use Modules\Order\Models\Carretatracto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Tenant\Establishment;
use App\Models\Tenant\Company;
use Illuminate\Support\Str;
use Modules\Finance\Helpers\UploadFileHelper;
class CarretatractoController extends Controller
{

    public function index()
    {
        return view('order::carretatractos.index');
    }

    public function columns()
    {
        return [
            'placa' => 'placa',
            'responsable' => 'responsable',
        ];
    }

    public function records(Request $request)
    {

        $records = Carretatracto::where($request->column, 'like', "%{$request->value}%")
                            ->orderBy('id');

        return new CarretatractoCollection($records->paginate(config('tenant.items_per_page')));
    }

    public function upload(Request $request)
    {

        $validate_upload = UploadFileHelper::validateUploadFile($request, 'file', 'jpg,jpeg,png,gif,svg');

        if(!$validate_upload['success']){
            return $validate_upload;
        }

        if ($request->hasFile('file')) {
            $new_request = [
                'file' => $request->file('file'),
                'type' => $request->input('type'),
            ];

            return $this->upload_image($new_request);
        }
        return [
            'success' => false,
            'message' =>  __('app.actions.upload.error'),
        ];
    }
    public function upload1(Request $request)
    {

        $validate_upload = UploadFileHelper::validateUploadFile($request, 'file', 'jpg,jpeg,png,gif,svg');

        if(!$validate_upload['success']){
            return $validate_upload;
        }

        if ($request->hasFile('file')) {
            $new_request = [
                'file' => $request->file('file'),
                'type' => $request->input('type'),
            ];

            return $this->upload_image1($new_request);
        }
        return [
            'success' => false,
            'message' =>  __('app.actions.upload.error'),
        ];
    }
    public function upload2(Request $request)
    {

        $validate_upload = UploadFileHelper::validateUploadFile($request, 'file', 'jpg,jpeg,png,gif,svg');

        if(!$validate_upload['success']){
            return $validate_upload;
        }

        if ($request->hasFile('file')) {
            $new_request = [
                'file' => $request->file('file'),
                'type' => $request->input('type'),
            ];

            return $this->upload_image2($new_request);
        }
        return [
            'success' => false,
            'message' =>  __('app.actions.upload.error'),
        ];
    }
    public function upload3(Request $request)
    {

        $validate_upload = UploadFileHelper::validateUploadFile($request, 'file', 'jpg,jpeg,png,gif,svg');

        if(!$validate_upload['success']){
            return $validate_upload;
        }

        if ($request->hasFile('file')) {
            $new_request = [
                'file' => $request->file('file'),
                'type' => $request->input('type'),
            ];

            return $this->upload_image3($new_request);
        }
        return [
            'success' => false,
            'message' =>  __('app.actions.upload.error'),
        ];
    }
    private function setFilename($expense){

        $name = ["check1",$expense->id,date('Ymd')];
        $expense->filename = join('-', $name);
        $expense->external_id = join('-', $name);
        $expense->save();
        
       
    }
    function upload_image1($request)
    {
        $file = $request['file'];
        $type = $request['type'];

        $temp = tempnam(sys_get_temp_dir(), $type);
        file_put_contents($temp, file_get_contents($file));

        $mime = mime_content_type($temp);
        $data = file_get_contents($temp);

        return [
            'success' => true,
            'data' => [
                'filename1' => $file->getClientOriginalName(),
                'temp_path1' => $temp,
                'temp_image1' => 'data:' . $mime . ';base64,' . base64_encode($data)
            ]
        ];
    }
    function upload_image($request)
    {
        $file = $request['file'];
        $type = $request['type'];

        $temp = tempnam(sys_get_temp_dir(), $type);
        file_put_contents($temp, file_get_contents($file));

        $mime = mime_content_type($temp);
        $data = file_get_contents($temp);

        return [
            'success' => true,
            'data' => [
                'filename' => $file->getClientOriginalName(),
                'temp_path' => $temp,
                'temp_image' => 'data:' . $mime . ';base64,' . base64_encode($data)
            ]
        ];
    }
    function upload_image2($request)
    {
        $file = $request['file'];
        $type = $request['type'];

        $temp = tempnam(sys_get_temp_dir(), $type);
        file_put_contents($temp, file_get_contents($file));

        $mime = mime_content_type($temp);
        $data = file_get_contents($temp);

        return [
            'success' => true,
            'data' => [
                'filename2' => $file->getClientOriginalName(),
                'temp_path2' => $temp,
                'temp_image2' => 'data:' . $mime . ';base64,' . base64_encode($data)
            ]
        ];
    }
    function upload_image3($request)
    {
        $file = $request['file'];
        $type = $request['type'];

        $temp = tempnam(sys_get_temp_dir(), $type);
        file_put_contents($temp, file_get_contents($file));

        $mime = mime_content_type($temp);
        $data = file_get_contents($temp);

        return [
            'success' => true,
            'data' => [
                'filename3' => $file->getClientOriginalName(),
                'temp_path3' => $temp,
                'temp_image3' => 'data:' . $mime . ';base64,' . base64_encode($data)
            ]
        ];
    }
    public function tables()
    {
        $identity_document_types = IdentityDocumentType::whereActive()->get();
        $api_service_token = config('configuration.api_service_token');
        $company = Company::active();
        $establishment = Establishment::where('id', auth()->user()->establishment_id)->first();
        return compact('identity_document_types', 'api_service_token','company','establishment');
    }

    public function record($id)
    {
        $record = new CarretatractoResource(Carretatracto::findOrFail($id));

        return $record;
    }

    public function store(CarretatractoRequest $request)
    {
        

        $id = $request->input('id');
        $record = Carretatracto::firstOrNew(['id' => $id]);
        $record->fill($request->all());
        $temp_path = $request->input('temp_path');
        $temp_path1 = $request->input('temp_path1');
        $temp_path2 = $request->input('temp_path2');
        $temp_path3 = $request->input('temp_path3');
        if($temp_path) {

            $directory = 'public'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'items'.DIRECTORY_SEPARATOR;

            $file_name_old = $request->input('image');
            $file_name_old_array = explode('.', $file_name_old);
            $file_content = file_get_contents($temp_path);
            $datenow = date('YmdHis');
            $file_name ='-'.$datenow.'.'.$file_name_old_array[1];
            Storage::put($directory.$file_name, $file_content);
            $record->imagen1 = $file_name;

           



        }else if(!$request->input('imagen1') && !$request->input('temp_path') && !$request->input('image_url')){
            $record->imagen1 = 'imagen-no-disponible.jpg';
        }

        if($temp_path1) {

            $directory = 'public'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'items'.DIRECTORY_SEPARATOR;

            $file_name_old = $request->input('image1');
            $file_name_old_array = explode('.', $file_name_old);
            $file_content = file_get_contents($temp_path1);
            $datenow = date('YmdHis');
            $file_name ='-1'.$datenow.'.'.$file_name_old_array[1];
            Storage::put($directory.$file_name, $file_content);
            $record->imagen2 = $file_name;

           



        }else if(!$request->input('imagen1') && !$request->input('temp_path1') && !$request->input('image_url1')){
            $record->imagen2 = 'imagen-no-disponible.jpg';
        }

        if($temp_path2) {

            $directory = 'public'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'items'.DIRECTORY_SEPARATOR;

            $file_name_old = $request->input('image2');
            $file_name_old_array = explode('.', $file_name_old);
            $file_content = file_get_contents($temp_path2);
            $datenow = date('YmdHis');
            $file_name ='-2'.$datenow.'.'.$file_name_old_array[1];
            Storage::put($directory.$file_name, $file_content);
            $record->imagen3 = $file_name;

           



        }else if(!$request->input('imagen2') && !$request->input('temp_path2') && !$request->input('image_url2')){
            $record->imagen3 = 'imagen-no-disponible.jpg';
        }
        if($temp_path3) {

            $directory = 'public'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'items'.DIRECTORY_SEPARATOR;

            $file_name_old = $request->input('image3');
            $file_name_old_array = explode('.', $file_name_old);
            $file_content = file_get_contents($temp_path3);
            $datenow = date('YmdHis');
            $file_name ='-3'.$datenow.'.'.$file_name_old_array[1];
            Storage::put($directory.$file_name, $file_content);
            $record->imagen4 = $file_name;

           



        }else if(!$request->input('imagen3') && !$request->input('temp_path3') && !$request->input('image_url3')){
            $record->imagen4 = 'imagen-no-disponible.jpg';
        }
        $this->setFilename($record);
        $record->save();

        return [
            'success' => true,
            'message' => ($id)?'Carreta editado con éxito':'Carreta registrado con éxito',
            'id' => $record->id
        ];
    }
    public static function merge_inputs($inputs)
    {

        

        $values = [
           
            'external_id' => $inputs['id'] ? $inputs['external_id'] : Str::uuid()->toString(),
            
        ];

        $inputs->merge($values);

        return $inputs->all();
    }
    public function destroy($id)
    {

        $record = Carretatracto::findOrFail($id);
        $record->delete();

        return [
            'success' => true,
            'message' => 'Carreta eliminado con éxito'
        ];

    }

}
