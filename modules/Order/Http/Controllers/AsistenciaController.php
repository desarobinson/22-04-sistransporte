<?php
namespace Modules\Order\Http\Controllers;

use Modules\Order\Http\Requests\AsistenciaRequest;
use Modules\Order\Http\Resources\AsistenciaCollection;
use Modules\Order\Http\Resources\AsistenciaResource;
use App\Models\Tenant\Catalogs\IdentityDocumentType;
use App\Http\Controllers\Controller;
use Modules\Order\Models\Asistencia;
use Illuminate\Http\Request;

class AsistenciaController extends Controller
{

    public function index()
    {
        return view('order::asistencias.index');
    }

    public function columns()
    {
        return [
            'nombres' => 'nombres',
            
        ];
    }

    public function records(Request $request)
    {

        $records = Asistencia::where($request->column, 'like', "%{$request->value}%")
                            ->orderBy('nombres');

        return new AsistenciaCollection($records->paginate(config('tenant.items_per_page')));
    }


    public function tables()
    {
        $identity_document_types = IdentityDocumentType::whereActive()->get();
        $api_service_token = config('configuration.api_service_token');

        return compact('identity_document_types', 'api_service_token');
    }

    public function record($id)
    {
        $record = new AsistenciaResource(Ruta::findOrFail($id));

        return $record;
    }

    public function store(AsistenciaRequest $request)
    {

        $id = $request->input('id');
        $record = Asistencia::firstOrNew(['id' => $id]);
        $record->fill($request->all());
        $record->save();

        return [
            'success' => true,
            'message' => ($id)?'Ruta editado con éxito':'Ruta registrado con éxito',
            'id' => $record->id
        ];
    }

    public function destroy($id)
    {

        $record = Asistencia::findOrFail($id);
        $record->delete();

        return [
            'success' => true,
            'message' => 'Ruta eliminado con éxito'
        ];

    }

}
