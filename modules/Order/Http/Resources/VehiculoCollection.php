<?php

namespace Modules\Order\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class VehiculoCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($row, $key) {
            return [
                'id' => $row->id,
                'placa' => $row->placa,
                'marca' => $row->marca,
                'modelo' => $row->modelo,
                'cargautil' => $row->cargautil,
                'carreta' => $row->carreta,
                'color' => $row->color,
                'motor' => $row->motor,
                'serie' => $row->serie,
                'anofabri' => $row->anofabri,
                'created_at' => $row->created_at->format('Y-m-d H:i:s'),
                'updated_at' => $row->updated_at->format('Y-m-d H:i:s'),
            ];
        });
    }
}
