<?php

namespace Modules\Order\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VehiculoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'placa' => $this->placa,
            'marca' => $this->marca,
            'modelo' => $this->modelo,
            'cargautil' => $this->cargautil,
            'carreta' => $this->carreta,
            'color' => $this->color,
            'motor' => $this->motor,
            'serie' => $this->serie,
            'anofabri' => $this->anofabri,

        ];
    }
}
