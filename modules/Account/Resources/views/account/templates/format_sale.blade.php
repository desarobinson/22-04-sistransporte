<style>
    .text-center {
        text-align: center;
    }
    .font-weight {
        font-weight: bold;
    }
</style>
@php
$col_span = 25;
@endphp
<table>
    <tr>
        <td colspan="{{ $col_span }}">{{ $company['name'] }}</td>
    </tr>
 
  
       
    <tr>
        <td>
            FECHA EMISION 
        </td>
        <td>FECHA CANCELACION </td>
        <td>CODIGO COMPROBANTE</td>
        <td>SERIE</td>
        <td>NUMERO</td>
        <td>S/N MAQUINA REGISTRADORA</td>
        <td>R.U.C.</td>
        <td>EXONERADA</td>
       
        <td>VV</td>
        <td>IGV</td>
        <td>TOTAL</td>
       
    </tr>
    @foreach($records as $row)
    <tr>
        
        <td>{{ $row['date_of_issue'] }}</td>
        <td>  @if($row['document_type_id'] == '14')
                {{ $row['date_of_due'] }}
                @else
                {{ $row['date_of_issue'] }}
            @endif

            </td>
        <td>01</td>
        <td>{{ $row['series'] }}</td>
        <td>{{ $row['number'] }}</td>
        <td></td>
        <td>{{ $row['customer_number'] }}</td>
        <td></td>

      
        <td>{{ (in_array($row['document_type_id'],['01','03']) && in_array($row['state_type_id'],['09','11'])) ? 0 :  $row['total_taxed'] }}</td>
     

        
        <td>{{ (in_array($row['document_type_id'],['01','03']) && in_array($row['state_type_id'],['09','11'])) ? 0 :  $row['total_igv'] }}</td>
       
        <td>{{ (in_array($row['document_type_id'],['01','03']) && in_array($row['state_type_id'],['09','11'])) ? 0 :  $row['total'] }}</td>

    </tr>
    @endforeach
</table>
