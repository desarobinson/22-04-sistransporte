@extends('tenant.layouts.app')

@section('content')
@push('scripts')
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
$('#dataFabricatiei').datepicker({
    defaultDate: new Date()
   
});

$('#dataFabricatiei2').datepicker("setDate", new Date());
</script>

@endpush
    <div class="row">
        <div class="col-md-20">
            <div class="card card-primary">
                <div class="card-header">
                    <div>
                        <h4 class="card-title">Consulta Liquidacion</h4>
                    </div>
                </div>
                <div class="card-body">
                    <div>
                        <form action="{{route('reports.general.index')}}" class="el-form demo-form-inline el-form--inline" method="POST">
                            {{csrf_field()}}
                            {{-- <div class="el-form-item col-xs-12">
                                <div class="el-form-item__content">
                                    <button class="btn btn-custom" type="submit"><i class="fa fa-search"></i> Buscar</button>
                                </div>
                            </div> --}}
                        </form>
                    </div>
                   
                    <div class="box">
                        <div class="box-body no-padding">
                        
                            <div style="margin-bottom: 10px" class="row">

                                <div style="padding-top: 0.5%" class="col-md-14">
                                
                                    <form action="{{route('reports.general.index')}}" method="get">
                                        {{csrf_field()}}
                                        
                                         <input  id="dataFabricatiei" name="ruc" type="hidden" value=" {{$company->number }}"/>
                                        <div class="row">
                                            <div class="col-md-3">
                                           <b> Vehiculo </b>
                                                <select class="form-control" name="warehouse_id" id="" style="width: 200px;">
                                                    <option {{ request()->warehouse_id == 'all' ?  'selected' : ''}} selected value="all">Todos</option>
                                                    @foreach($vehiculos as $item)
                                                    <option {{ request()->warehouse_id == $item->id ?  'selected' : ''}} value="{{$item->placa}} - {{$item->marca}}">{{$item->placa}} - {{$item->marca}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                            
                                            <b> Fecha inicio </b>
                                        <input  id="dataFabricatiei" name="dataFabricatiei" type="date" style="border: 1px solid #ced4da;
    border-radius: 0.25rem;
    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;height: 37px;" value="{{request()->dataFabricatiei}}" />
                                            </div>
                                            <div class="col-md-3">
                                            
                                            <b> Fecha Final </b>
                                        <input  id="dataFabricatiei2" name="dataFabricatiei2" type="date"  style="border: 1px solid #ced4da;
    border-radius: 0.25rem;
    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;height: 37px;" value="{{request()->dataFabricatiei2}}"/>
                                            </div>
                                            <div class="col-md-2"> <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> Buscar</button></div>
                                        </div>
                                       
                                        
                                    </form>
                                </div>
                                @if(isset($reports))
                                    <div class="col-md-12">
                                        <form action="{{route('reports.general.pdf')}}" class="d-inline" method="POST">
                                            {{csrf_field()}}

                                            <input  id="ruc" name="ruc" type="hidden" value=" {{$company->number }}"/>
                                           
                                            <input  id="dataFabricatiei" name="dataFabricatiei" type="date" 
                                            value="{{request()->dataFabricatiei}}"
                                            style="border: 1px solid #ced4da;
    border-radius: 0.25rem;
    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;height: 37px;   display: none;"  />
                                            <input  id="dataFabricatiei2" name="dataFabricatiei2" type="date" 
                                            value="{{request()->dataFabricatiei2}}"
                                            style="border: 1px solid #ced4da;
    border-radius: 0.25rem;
    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;height: 37px; display: none;"  />
                                            <input type="hidden" name="warehouse_id" value="{{request()->warehouse_id ? request()->warehouse_id : 'all'}}">
                                            <button class="btn btn-custom   mt-2 mr-2" type="submit"><i class="fa fa-file-pdf"></i> Exportar PDF</button>
                                            {{-- <label class="pull-right">Se encontraron {{$reports->count()}} registros.</label> --}}
                                        </form>

                                        <form action="{{route('reports.general.report_excel')}}" class="d-inline" method="POST">
                                            {{csrf_field()}}
                                            <input  id="ruc" name="ruc" type="hidden" value=" {{$company->number }}"/>
                                           
                                            <input  id="dataFabricatiei" name="dataFabricatiei" type="date" 
                                            value="{{request()->dataFabricatiei}}"
                                            style="border: 1px solid #ced4da;
    border-radius: 0.25rem;
    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;height: 37px;   display: none;"  />
                                            <input  id="dataFabricatiei2" name="dataFabricatiei2" type="date" 
                                            value="{{request()->dataFabricatiei2}}"
                                            style="border: 1px solid #ced4da;
    border-radius: 0.25rem;
    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;height: 37px; display: none;"  />
                                            <input type="hidden" name="warehouse_id" value="{{request()->warehouse_id ? request()->warehouse_id : 'all'}}">
                                            <button class="btn btn-custom   mt-2 mr-2" type="submit"><i class="fa fa-file-excel"></i> Exportar Excel</button>
                                            {{-- <label class="pull-right">Se encontraron {{$reports->count()}} registros.</label> --}}
                                        </form>
                                    </div>

                                @endif


                            </div>
                            <table width="120%" class="table table-striped table-responsive-xl table-bordered table-hover" >
                                <thead class="">
                                    <tr>
                                        <th>#</th>
                                        <th>Numero</th>
                                        <th>Fecha</th>
                                        <th>Cliente</th>
                                        <th>Servicio/Carga</th>
                                        <th>Gastos de Viaje</th>
                                        <th>otros Gastos</th>
                                        <th>Total</th>
                                        <th>Galones</th>
                                        
                                        <th>Total combustible</th>
                                        <th>Comprobante</th>
                                        <th>Monto Flete </th>
                                        <th>Destraccion </th>
                                        <th>Estibaje </th>
                                        <th>Total</th>
                                                                                                            
                                        
                                    </tr>
                                </thead>
                                @php 
                                $totalrepuesto=0;
                                @endphp
                                <tbody>
                                    @foreach($reports as $key => $value)
                                    <tr>
                                        <td class="celda">{{$loop->iteration}}</td>
                                        <td class="celda">{{$value->id}}</td> 
                                        <td class="celda">{{$value->date_of_issue}}</td> 
                                        
                                        <td class="celda">{{$value->cliente}}</td> 
                                        <td class="celda">{{$value->servicio}}</td>  
                                        <td class="celda">{{$value->gv}}</td>  
                                        <td class="celda">{{$value->go}}</td>  
                                          
                                        <td class="celda">{{$value->go+$value->gv}}</td>  
                                        <td class="celda">0</td>                                       
                                        
                                        <td class="celda">{{$value->lima+$value->hyo}}</td>
                                        <td class="celda">{{$value->fac}}</td> 
                                        <td class="celda">{{$value->total}}</td>  
                                        <td class="celda">{{$value->total * 0.04}}</td>                                       
                                         <td class="celda">{{$value->estibaje}}</td>
                                         <td class="celda">{{$value->total - $value->estibaje - $value->go-$value->gv-$value->lima - $value->hyo - ($value->total *0.04)}}</td>
                                    </tr>
                                    @php 
                                    $totalrepuesto=$value->repuesto;
                                @endphp
                                    
                                    @endforeach
                                </tbody>
                            </table>
                            <table>
                                <tr>                   
                                <td><b>Total Repuestos</b></td>            
                                <td><b>S/. {{ $totalrepuesto}}</b></td>
                                </tr>
                                
                            </table>


                            Total {{$reports->total()}}
                            <label class="pagination-wrapper ml-2">
                                {{$reports->appends($_GET)->render()}}
                            </label>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
@endsection


