@php
    $establishment = $document->establishment;
    $supplier = $document->supplier;
    
    $tittle =str_pad($document->id, 8, '0', STR_PAD_LEFT); 
@endphp
<html>
<head>
    {{--<title>{{ $tittle }}</title>--}}
    
    
    </style>
</head>
<body>
<div class="ticket">
<table class="full-width">
    <tr>
       
        <td width="50%" >
            <div class="text-center">
            <h5 class="text-center">.</h5>
                <h4 class=""><b>{{$company->name }}</b></h4>
                <h5><b>{{'RUC '.$company->number }}</b></h5>
                
            </div>
        </td>
       
    </tr>
    <tr>
    <td>
    <h5 class="text-center">.</h5>
            <h5 class="text-center">Tipo Comprobante :{{ $document->document_type->description}}</h5>
            <h5 class="text-center"><b>Nro de Comrprobante: {{ $tittle }}</b></h5>
            <h5 class="text-center"><b>Nro de Orden: {{ $document->order_id}}</b></h5>
     </td>
    </tr>
    
</table>
<table class="full-width mt-5">
    <tr>
        <td width="30%">Proveedor:</td>
        <td width="40%">{{ $supplier->name }}</td>
        
    </tr>

    <tr>
        <td>{{ $supplier->identity_document_type->description }}:</td>
        <td>{{ $supplier->number }}</td>
        @if($document->date_of_due)
            <td width="25%">Fecha de vencimiento:</td>
            <td width="15%">{{ $document->date_of_due->format('Y-m-d') }}</td>
        @endif
    </tr>
    <tr>
         <td width="30%">Fecha:</td>
        <td width="60%">{{ $document->date_of_issue->format('Y-m-d') }}</td>
    </tr>
    @if ($supplier->address !== '')
    <tr>
        <td class="align-top">Dirección:</td>
        <td colspan="3">
            {{ $supplier->address }}
            {{ ($supplier->district_id !== '-')? ', '.$supplier->district->description : '' }}
            {{ ($supplier->province_id !== '-')? ', '.$supplier->province->description : '' }}
            {{ ($supplier->department_id !== '-')? '- '.$supplier->department->description : '' }}
        </td>
    </tr>
    @endif 
    @if ($supplier->telephone)
    <tr>
        <td class="align-top">Teléfono:</td>
        <td colspan="3">
            {{ $supplier->telephone }}
        </td>
    </tr>
    @endif 
    <tr>
        <td class="align-top">Usuario:</td>
        <td colspan="3">
            {{ $document->user->name }}
        </td>
    </tr>
    <tr>
        <td class="align-top">Placa:</td>
        <td colspan="3">
            {{ $document->placa }}
        </td>
    </tr>
   
</table>
 

<table class="full-width mt-10 mb-10">
    <thead class="">
    <tr class="bg-grey">
        
        
        <th class="border-top-bottom text-left py-2" width="46%">Descripcion</th>
        <th class="border-top-bottom text-right py-2" width="10%">Gal.</th>
        <th class="border-top-bottom text-center py-2" width="15%">Precio</th>
        <th class="border-top-bottom text-left py-2" width="19%">Total</th>
    </tr>
    </thead>
    <tbody>
    @foreach($document->items as $row)
        <tr>
            <td class="align-top">
            {{ $row->description}}
            </td>
            <td class="text-left align-top">{{ $row->galones}}</td>
            <td class="text-left align-top">{{ number_format($row->precio, 2) }}</td>
            
            <td class="text-left align-top">{{ number_format($row->total, 2) }}</td>
        </tr>
        <tr>
            <td colspan="6" class="border-bottom"></td>
        </tr>
    @endforeach
       
    </tbody>
</table>

<table class="full-width" style="margin-top:40px">
    <tr>
         
            <td colspan="6" class="border-bottom"></td>
        
    </tr>
    <tr>
         
            <td colspan="6" style="margin-left:20px"><b> FIRMA DE PROVEEDOR</b></td>
        
    </tr>
</table>
</div>
</body>
</html>
